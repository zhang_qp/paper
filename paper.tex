\documentclass{llncs}

\usepackage{amsmath}
\usepackage{llncsdoc}
\usepackage{enumerate}
\usepackage{url}
\usepackage{breakurl}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{listings}

\usepackage{wrapfig}

\begin{document}
\title{SPMP: A Shared Persistent Memory Parallel Programming Model for Node.js}

\author{Qipeng Zhang\inst{1} \and Hao Xu\inst{1} \and Tianyou Li\inst{2} \and Pan Deng\inst{2} \\ \and Yuting Chen\inst{1} \and Linpeng Huang\inst{1} \and Andy Rudoff\inst{3} }
\institute{Shanghai Jiao Tong University, Shanghai, China\\
\{zqp19941019, xuhao2012\}@sjtu.edu.cn \\
\{chenyt, lphuang\}@cs.sjtu.edu.cn
\and
Intel Asia Pacific R\&D Co. LTD, Shanghai, China  \\
\{tianyou.li, pan.deng\}@intel.com, \\
\and
Intel Corporation, Santa Clara, CA, USA \\
andy.rudoff@intel.com
 }

\maketitle
%
\begin{abstract}
    JavaScript is widely used for scripting on client side. Node.js is a JavaScript runtime environment, allowing Javascript to be used for building scalable network applications on server side. However, Node.js is weak in parallel programming, which is limited to enhance JavaScript's performance. Meanwhile, persistent memory (PM) shows optimistic prospects of being used in server-side applications, while few researches do exist on allowing script languages to support PM.
    In this paper, we introduce \emph{SPMP}, a new parallel programming model for JavaScript. \emph{SPMP} combines parallelism with our newly designed PersistentArrayBuffer, which extends JavaScript ArrayBuffer to support operations on persistent memory. \emph{SPMP} allows multi-processes to share persistent memory with optional dynamic load-balancing strategies and ensures object coherency. With \emph{SPMP}, we can also promise persistence to secondary storage. We evaluated \emph{SPMP} against Extended Memory Semantics (EMS, another node.js module for supporting shared persistent memory parallel programming). Experiments show that \emph{SPMP} outperforms EMS by a factor of 100 - 500 for five basic operations, owing to the different ways of file mapping.\\

\textbf{Keywords}: Node.js, parallel programming, shared persistent memory, PersistentArrayBuffer.
\end{abstract}

\section{Introduction}
As a dynamically typed, prototype-based, object-oriented language, JavaScript can be interpreted and JIT'ed by JavaScript engines embedded in browsers, such as V8 in Chrome \cite{V8}, SpiderMonkey\footnote{https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey} in Firefox and Nitro in Safari based on SquirrelFish\footnote{https://webkit.org/blog/214/introducing-squirrelfish-extreme/}. Thus it plays an important role in websites and web applications~\cite{JS_Usage}. At the same time, features have made JavaScript flexible and popular, while makes it difficult for developers to efficiently optimize their programs~\cite{selakovic2016performance}\cite{wei2017system}\cite{chevalier2015interprocedural}. In order to extend JavaScript to support long-running server processes, Node.js \cite{Nodejs} is created based on V8 engine, the Google's runtime implementation. It provides an open-source, cross-platform runtime environment for executing JavaScript codes at server side. Owing to the functional nature that JavaScript supports event callbacks, Node.js is designed to use an asynchronous I/O eventing model instead of multi-threading. However, Node.js runs in a single process and never executes its business in parallel \cite{tilkov2010node}. The minimal support for parallel programming is still a limitation for data-intensive applications and so-called microservices \cite{viennot2015synapse}. Meanwhile, Persistent memory(PM), which merges memory and storage tiers of computer, has attracted attention of both academia and industry since its appearance. Persistent memory is accessed like volatile memory with load and store instructions, but it retains its contents when faced with power loss or application crush, which is similar to storage. Persistent Memories, such as Phase Change Memory (PCM)\cite{raoux2008phase}, STT-RAM \cite{dieny2008spin} and RamDisk \cite{nielsen1999use}, promise several exciting features, including DRAM-like access speed, persistence, high data endurance, retention and byte-addressability. These technologies exhibit totally different characteristics compared to traditional memory and storage technologies. Byte-addressable PM devices are expected to be commercially available in the foreseeable future.

\indent Bringing parallel executions to a non-parallel language is a great challenge. There are some researches on the topic, for example, WebWorkers \cite{WebWorker} is a parallelism model for JavaScript on  both client and server sides, and it is based on share-nothing parallel workers, which means that developers need to explicitly partition and distribute data, and the workers can only exchange data with help of external tools. GEMs \cite{bonetta2016gems}\cite{salucci2016generic} is another model which combines message passing and shared-memory parallelism, and thus extends the classes of parallel applications built on Node.js. However, it is based on the traditional RAM as the shared memory, and cannot take use of the features of persistent memory. To the best of our knowledge, Extended Memory Semantics (EMS)\footnote{Jace A Mogill: http://syntheticsemantics.com/EMS.js} is the only model which supports parallel programming for Node.js and promises persistence to PM at the same time. EMS implements a shared address space with a rich set of primitives for parallel access of data structures, and it leverages existing tool chains instead of replacing them. However, EMS is based on Node.js addon nan, which means that any JavaScript operation should be first translated to addon function, then to a C/C++ operation. This is the main bottleneck of EMS.

\indent In this paper, we introduce a new parallel programming model called \emph{SPMP}, specifically designed to enable shared persistent memory for Node.js. Considering the limits of EMS, we seek for a solution which bypasses nan and visits the persistent memory directly. This paper makes the following contributions:
\begin{enumerate}
  \item We design \emph{SPMP}, a new Node.js parallel programming model allowing multi-processes to share objects. \emph{SPMP} brings parallelism to JavaScript and takes full use of the CPUs.
  \item We address several main challenges of parallel programming, including object coherency, synchronization, dynamic load-balancing. At the same time, \emph{SPMP} can also support persistence both for persistent memory and secondary storages.
  \item We implement \emph{SPMP} on data-intensive Node.js tasks. We evaluate the read, write, copy, add and fetch\&add operations on \emph{SPMP} and EMS, and results show that \emph{SPMP} outperforms EMS by a factor of 100 - 500.
\end{enumerate}

The remainder of this paper is structured as follows. In section 2, we introduce more background information. Section 3 is the main part showing the design details and tricks used to optimize the model. In section 4, several experiments are conducted to show performance of \emph{SPMP}. Section 5 makes the conclusions.

\section{Background}
%JavaScript is ubiquitous on the web. Dubbed as ``Assembly Language of the Internet", JavaScript is not only used for webpages and online applications, but also taken as the target for applications written in other languages, such as Java and HTML. Unfortunately, JavaScript does not directly support parallelism in the current standard.

%\emph{A. Node.js}\\
%\indent Node.js is a JavaScript runtime built on Chrome's V8 engine \cite{V8}. Although JavaScript is mainly used to develop client-side web applications, Node.js is designed to extend it to build scalable network applications on server side. Contrast to most concurrency models where threads are employed, Node.js uses an event-driven, non-blocking I/O model, which makes it lightweight and efficient. Considering that there is no block, scalable systems are very reasonable to be developed with Node.\\
\noindent\textbf{Node.js addon}
Node.js addons are dynamically-linked shared objects written in C++. Similar to other ordinary Node.js modules, addons can be loaded into Node.js with \texttt{require()} function. With the addons, an interface between JavaScript running in Node.js and C/C++ libraries is created, which means that the C/C++ functions are exported to be used in JavaScript. Now there are two main different ways to develop addons, which are Native Abstractions for Node.js (nan) and N-API\footnote{Node.js addons: http://nodejs.cn/api/en/addons.html}.

\noindent\textbf{ArrayBuffer and TypedArray}
 ArrayBuffer \cite{ArrayBuffer} is a JavaScript object used to operate a generic, fixed-length raw binary data buffer, and created by function \texttt{new ArrayBuffer()} with a size in bytes. Appearance of ArrayBuffer enhances JavaScript's ability to deal with binary byte streams, and it is more efficient than arrays. Contents of an ArrayBuffer cannot be manipulated directly. Instead, a typed array object is necessary. A TypedArray object describes an array-like view of the ArrayBuffer, and underlying elements can then be referenced with array index.

%\noindent\textbf{Extended Memory Semantics}
% To our knowledge of Node.js parallelism, Extended Memory Semantics (EMS)\cite{EMS} is the only model which supports parallel programming for Node.js and promises persistence to PM and secondary storage. EMS implements a shared address space with a rich set of primitives for parallel access of data structures, and it leverages existing tool chains instead of replacing them. EMS is targeted at tasks that are too small for a cluster, and too large for one process. For data exchange, EMS uses JSON to implement serialization and deserialization of data types, which is one of the main sources of time cost.

\noindent\textbf{PMDK}
 The Persistent Memory Development Kit (PMDK)\footnote{Intel: http://pmem.io/} is a growing collection of libraries and tools for persistent memory. It is developed and maintained by Intel Corporation. The libraries utilize DAX features of both Linux and Windows to allow applications to access persistent memory as memory-mapped files. With PMDK, we can take advantage of persistent memory efficiently and work on any product that provides the persistent memory programming model. Now PMDK does not support multi-process programming.

\section{Design and Implementation}
\emph{SPMP} is a new model that allows processes to share persistent memory with Node.js. Figure\ref{figure1} shows the main components of \emph{SPMP} and some fundamental strategies. In CPU, new processes are forked from the main process. In persistent memory, each process has its own address space mapped to an shared file with PersistentArrayBuffer. A shared parameters file is used for communication among processes, and it should be visited with PABs, too.

In this section, we will describe the design details of main techniques in \emph{SPMP} model. In addition, we will introduce several parallel algorithms to ensure load-balancing.


\begin{figure}[t]
    \centerline{ \includegraphics[width=13cm, height=7cm]{figures//structure.pdf}}
    \caption{Overview of SPMP model.}
    \label{figure1}
\end{figure}

\subsection{PersistentArrayBuffer}
PersistentArrayBuffer (PAB) is a new JavaScript interface designed by us in a similar way to ArrayBuffer(AB), and used to represent a generic, fixed-length raw binary data buffer. Although ArrayBuffer is efficient enough to be used to operate on RAM, it cannot take use of the features of persistent memory, and thus PAB is necessary. Except for the support of PM, another difference between the two interfaces lie on the ways of memory allocation and mapping.

In order to ensure persistence, using a file on disk or on PM as backup of memory is a widely-recognized method. C function \texttt{mmap()} is a common chioce to map a file or other object to memory. We consider to expose the \texttt{mmap()} to JavaScript so that it can be directly used. In this aspect, Native Abstractions for Node.js (nan) can function effectively. However, nan is based on node.js instead of native to V8, which means that it is obviously slower to translate functions and data from JavaScript to C than native ways. This is the main reason why we abandon nan and seek for another method. We find Persistent Memory Development Kit (PMDK), which contains a series of persistent memory libraries. In our design, V8 manages PAB in the same way as AB, except for memory allocation. Instead of \texttt{malloc()} used by AB, PAB takes advantage of PMDK to allocate, manage and release memory. PMDK takes memory-mapped files as persistent memory for a PM aware file system, and PAB implements persistence in the same manner.

We list functions and properties of PAB in Table \ref{table1}. The main function of PAB is \texttt{new Persistent\-Array\-Buffer(length, path, mode)}, which is used to create a PAB object or open an existing one. In the function, there are three main parameters: \emph{length} is the size of the persistent array buffer in bytes to be created or opened; \emph{path} is the absolute path of file to be mapped, which is on the persistent memory or disk; \emph{mode} is optional to be ``c" or ``o", which correspond to creating or opening a PAB object. Same as ArrayBuffer, PAB cannot be directly operated but with TypedArray objects. Elements in the array can be referenced using standard array index syntax. There are several optional TypedArray objects, and we use \texttt{Uint8Array}, \texttt{Uint16Array} and \texttt{Uint32Array} in this paper.
\begin{table}[t]
    \caption{Functions and properties of PAB.}
    \begin{tabular}{ll}
    \hline\noalign{\smallskip}
    Functions \& Properties&Description\\
    \noalign{\smallskip}
    \hline
    \noalign{\smallskip}
    \texttt{new Persistent\-Array\-Buffer(length,} & Create a new PAB object with given \\
    \texttt{path, mode)} & parameters\\
    \texttt{Persistent\-Array\-Buffer.length}& The PAB constructor's length property\\
    \texttt{get Persistent\-Array\-Buffer[@@species]}& The constructor function of derived objects\\
    \texttt{Persistent\-Array\-Buffer.prototype} &Add properties to all PAB objects\\
    \hline
    \end{tabular}
    \label{table1}
\end{table}

 Now we have PAB, an easy-to-use JavaScript interface to operate on persistent memory. Considering the art of persistence, it naturally comes to our mind that the memory-mapped files can be shared among several PABs, which further inspires us to realize a shared-memory parallel programming model for Node.js.

\subsection{Shared objects}
Informally, a \emph{SPMP} is a runtime mechanism to mediate access to a shared object graph between two or more parallel workers, which are isolated parallel entities. In this way, we can leverage the CPU cores and resources better. To realize parallel programming, we have to answer three main questions:
\begin{enumerate}
    \item How the child processes can be generated?
    \item How to share an object among several processes?
    \item How the processes communicate with each other?
\end{enumerate}
\subsubsection{Child Process.}
%child\_process provides the ability to spawn child processes. In addition, it also establishes pipes between the parent Node.js process and the spawned child one in a non-blocking manner. Function \emph{child\_process.spawn()} provides the spawn ability, and some other synchronous and asynchronous alternatives, such as \emph{child\_process.exec()}, \emph{child\_process.fork()}, \emph{child\_process.execSync()}, are implemented on top of it to provide specific functionalities.\\
Node.js module child\_process provides the ability to spawn child processes. In our design of this parallel programming model, the PAB module is wrapped as an independent module that can be invoked with Node.js function \texttt{require()} in other JavaScript files. In order to run modules in the child processes, we choose to use function \texttt{child\_process.fork()}, which takes module path and list of arguments as parameters. The function is wrapped for the main process to fork same number of sub\_processes as input argument.

Node.js child processes are independent of the parent except the communication channel that is established between the two. Each of them has its own memory, JS heap and V8 instances. Tasks to be conducted on each process is assigned with the first parameter of \texttt{child\_process.fork()}, and they need to be further partitioned with specific parallel strategies.
\subsubsection{Shared memory-mapped files.}
As we mentioned in the introduction of PMDK, persistence of PMDK and PAB is implemented with memory-mapped files. Sharing persistent memory can now be equivalently realized by sharing the same memory-mapped file among different processes. In addition to PM, considering that PAB can also be mapped to files on the disk, persistence to the secondary storage is also provided in a similar way.

Specifically, for a case where there are four different processes, we intend to assign a PAB for each process, and the four PABs are mapped to the same file on the persistent memory or disk. The design can be realized with the following steps:

\begin{enumerate}
  \item The main process creates a PAB object with function \texttt{new Persistent\-Array\-Buffer(length, path, "c")}, where length is related to the number of operations and TypedArray used. Considering that the basic unit of PAB's length is "byte", the relationship should be:
\begin{equation}
length = operations * (type / 8)
\label{eq1}
\end{equation}

  \item Generate three child processes with function \texttt{child\_process.fork()} on the main process, and assign them with script and other parameters;

  \item On each child process, open the PAB object created with function \texttt{new Persistent\-Array\-Buffer(length, path, "o")}, where length and path are the same as the main process.
\end{enumerate}


\subsubsection{Shared common parameters.}
The mapped file can now be shared among different processes with PAB, it is necessary to establish communication to avoid some common problems in parallel programming. For example, if a write-to-shared\_file task is partitioned across several processes, how can each process know its own part?

To solve these problems, we come up with an effective solution, which is also based on PAB. A file called "shared parameters file" is created and can be mapped to the address space of each process with PABs. It saves some basic parameters listed in Table \ref{table2}, and  changes of these shared parameters are synced to the file, so that they can be broadcasted to all processes. Actually, although this new shared file and corresponding PABs make it easy to share the common parameters, it brings some new problems. We will discuss how these parameters change and how to avoid the problems in detail in the following two subsections.
\begin{table}[t]
    \caption{Definition of parameters.}
    \centering
    \begin{tabular}{ll}
    \hline\noalign{\smallskip}
    Parameter & Function\\
    \noalign{\smallskip}
    \hline
    \noalign{\smallskip}
    NPROC & Number of processes\\
    BAR & Processes not at barrier \\
    FLAG & Barrier state\\
    INDEX & Start index of task\\
    END & End index of task\\
    SIZE & Designated chunk size\\
    MINSIZE & Minimum chunk size\\
    STRATEGY & Task decomposition strategy\\
    \hline
    \end{tabular}
    \label{table2}
\end{table}

\subsection{BSP and parallel algorithms}
\begin{wrapfigure}{R}[0cm]{0pt}
\includegraphics[width=3cm, height=5cm]{figures//BSP.pdf}
\caption{Bulk synchronous parallel.}
\label{figure2}
\vspace*{-1pc}
\end{wrapfigure}

The bulk synchronous parallel (BSP) is the bridging model we choose to design parallel algorithms. Figure\ref{figure2} shows how five processes run a program with several tasks: all processes enter the program at the main entry point, conduct their own works assigned by parallel algorithms, and synchronize at a barrier for each task. With BSP, we can efficiently count the execution time of each task, and all the processes are controlled to work on the same task in a certain time interval.

Barrier is an important part of BSP. For two contiguous tasks, the latter one will not be conducted until all the processes arrive at the barrier between them. To implement barrier, parameters NPROC, BAR and FLAG are used. BAR is initialed to be equal to NPROC and used to record the number of processes that have not arrived at the barrier, and it minus one each time a process arrives. Before the last process arrives, all the other arrived processes repeatedly sleep 1 millisecond. If no error occurs, BAR will be reduced to 0, which informs the last process to inverse FLAG and initialize value of BAR. At the same time, all other processes will break the loop of sleep.

To ensure load balancing among processes, on the base of BSP, we provide three different scheduling options, each takes use of one or more shared parameters:
\begin{enumerate}
    \item \emph{``static"} Intuitively, evenly dividing each task for all processes is a basic option.
    \item \emph{``preempt"} Similar to static, divide each task into (4 * NPROC) parts evenly, and then the processes preempt these parts when they are free.
    \item \emph{``guided"} Dynamically assign a chunk size (shared parameter SIZE) according to the total size of remaining task to do. In our design, each time the chunk size is calculated as (2). And then the free processes will preempt this chunk.

    \begin{equation}
    \resizebox{.7\hsize}{!}{$SIZE=max((END-INDEX)/(2*NPROC), MINSIZE)$}
    \label{eq2}
    \end{equation}
\end{enumerate}

Both \emph{preempt} and \emph{guided} allow processes to preempt the task chunks. Taking advantage of PAB, this can be easily realized. For example, if there is a write-to-file task with 200 write operations, INDEX and END are initialized as 0 and 199; For each chunk, its start is INDEX and end is INDEX+SIZE, then the chunk can be preempted; At the same time, the INDEX is now updated to INDEX+SIZE and SIZE is changed according to different scheduling options. Considering that the parameters are shared, changes should be broadcast to all processes, and another chunk can now be assigned and preempted.

\subsection{Concurrency control}
The main challenge in designing concurrent programs is concurrency control, which aims to ensure the correct sequencing of the interactions or communications among different processes, and coordinate access to shared resources. Without concurrency control, the shared resources may be changed unexpectedly, and thus cause deadlocks or race conditions. As a common and vital part of concurrent programming, our \emph{SPMP} model cannot ignore concurrency control.

Considering the realization of shared persistent memory mentioned in the above sections, concurrency control is needed when the shared parameters are changed. For example, two processes (denote as Proc1 and Proc2) preempt a chunk which ranges from INDEX to INDEX+SIZE of task index. As expected, if PROC1 successes, it will perform the operations and value of INDEX should immediately updated to INDEX+SIZE. However, before INDEX being changed and synced to the mapped parameter file by Proc1's PAB, Proc2 has already fetched the original value of INDEX and SIZE, so now Proc2 will operate on the same task index range, again. This may cause dirty read, overwriting or write contradictions.

Atomic operations like \texttt{compare-and-swap} and \texttt{fetch-and-add} are commonly used to implement concurrency control, and a series of C functions has been provided, including \texttt{\_\_sync\_fetch\_and\_add()}, \texttt{\_\_sync\_fetch\_and\_and()} and so on. N-API can expose C/C++ functions to JavaScript functions. Specifically for Node.js, where there is no pointer and variable type, some changes to the function prototype are necessary. N-API addon is written in C, so it can directly call the native C function, which is \texttt{type \_\_sync\_fetch\_and\_add(type *ptr, type value)}. As to the problem of pointer \texttt{*ptr} used in the function, considering that our design is based on SharedArrayBuffer, it can be replaced with an array and an offset in JavaScript. So the corresponding JS function prototype is \texttt{var \_\_sync\_fetch\_and\_add(Array, offset, value)}.

There are several steps to design and apply N-API to the model. Firstly, the function arguments are fetched and transferred to proper types. Considering that N-API function \texttt{napi\_get\_buffer\_info(env, args[0], buffer, buffer\_length)} is designed to take a \texttt{(void *)buffer} as an argument, it should be transferred to type \texttt{(int *)} because the fetched array from JavaScript is TypedArray. Secondly, an object is created with native C function \texttt{\_\_sync\_fetch \\ \_and\_add()}, and exported to the addon module. Then, a binding.gyp file should be created to describe the source addon file and name of the target module. The last step is to compile the source file and binding.gyp with Node.js tool node-gyp, and a binary node file is generated as a result. Now we can directly import the binary addon with function \texttt{require()} in Node.js files, and call the functions wrapped in the addon.

\section{Evaluation}
\subsection{Experiment Setup}
To evaluate the performance benefits of \emph{SPMP} model, we design a test.js file containing five basic operations listed in Table \ref{table3}, where \emph{idx} is an index of ArrayBuffer a, b or c. In each test, there are five main tasks to be conducted on multiple processes with a bulk synchronous parallel style, and each task is to perform 20,000,000 times of one operation. For each task, there is a timer function to count its running time and a barrier function to synchronize all processes at the end. The running time recorded is actually from the mutual task entry to the arrive time of the last process. Experiments are designed to show how \emph{SPMP} takes advantage of multiple processes, how different parallel algorithms work, and how much \emph{SPMP} outperforms the benchmark -- Extended Memory Semantics (EMS).
\begin{table}[t]
    \caption{Basic operations in experiments.}
    \centering
    \begin{tabular}{ll}
    \hline\noalign{\smallskip}
    Operation &     Function\\
    \noalign{\smallskip}
    \hline
    \noalign{\smallskip}
    Write & a.write(idx, idx) \\
    Read & a.read(idx) \\
    Copy & b.write(idx, a.read(idx)) \\
    c=a*b   & c.write(idx, a.read(idx)*b.read(idx)) \\
    c+=a*b   & c.write(idx, c.read(idx)+(a.read(idx)*b.read(idx)) \\
    \hline
    \end{tabular}
    \label{table3}
\end{table}

In order to compare the performance of different models and load-balancing strategies, a common criterion is used, which is \emph{atomic operations per second} in our experiments. It can be calculated with total operations divided by time interval of each task. As expected, for each experiment, read should be the fastest, and \texttt{c+=a*b} be the slowest one. Each experiment is conducted 10 times, and the average results are shown in the figures.

The experiments have been run on a server-class machine equipped with 64GB RAM and a 4-core Intel(R) Core(TM) i7-7700K CPU @ 4.20GHz. The operating system is Ubuntu 17.04. PersistentArrayBuffer is newly designed by us and therefore it is not supported by any official node version. We compiled a new node version to support PAB based on node v9.2.1 and PMDK, and install on the machine. In addition, to verify that \emph{SPMP} model ensures persistence to persistent memory, we emulate a PM device with RAM and mount it with DAX to create a PM-aware environment. With the emulated PM, the performance hit caused by \texttt{msync()} would be eliminated.

\subsection{Multiple processes}
The main task of \emph{SPMP} model is to share the persistent memory among multiple processes with PersistentArrayBuffer. In addition, ensuring process synchronization and object coherency is also necessary. In this experiment, we run the test with different number of processes. As the basic experiment, the parallel algorithm used here is \emph{``static"}, where each task is equivalently partitioned to all processes.

From the experiment results shown in Fig.\ref{figure3}, we verify two expectations. Firstly, as the number of processes increases, it takes less time to conduct the tasks. More accurately, there is linear correlation between the two, which means that \emph{SPMP} does work and all processes actually run at the same time. The other observation is that, time consumption of the five atomic operations increases in such an order: read, write, copy, \texttt{c=a*b} and \texttt{c+=a*b}. As to this observation, there is something interesting. On one hand, for read and write, the time gap should be larger in common case. Considering that the time interval is decide by the last arrived process, and there are some synchronization operations for read and write task, the result that the two takes similar time is reasonable. On the other hand, copy, \texttt{c=a*b}, and \texttt{c+=a*b} are combinations of read and write operations, so their time consumption is related to the number of read/write contained.
\begin{figure}[t]
    \centerline{ \includegraphics[width=8cm, height=5cm]{figures//static.pdf}}
    \caption{Operations per second for different processes.}
    \label{figure3}
\end{figure}
\subsection{Parallel algorithms}
In addition to \emph{``static"} used in the basic experiment, there are two other parallel algorithms designed to ensure load-balancing, which are \emph{``preempt"} and \emph{``guided"}. Same experiments are conducted with different number of processes for them. Figure\ref{figure4} show the results for \emph{``guided"} and \emph{``preempt"}.

From both figures, we can find that the linear correlation between number of processes and task time mentioned in the above subsection still exist. What's more, although both perform well, \emph{``guided"} is even better. Compare design of the two algorithms, \emph{``guided"} partitions each task into more but smaller chunks for the processes to preempt, and \emph{``preempt"} uses fewer chunks with larger size. The time interval is decided by when the last process finishes its task, so \emph{``guided"} can be more balanced and thus faster.
\begin{figure}
  \centering
  \subfigure[guided]{
    %\label{fig:subfig:a}%
    \includegraphics[width=5.8cm, height=4cm]{figures//guided.pdf}
  }
  \subfigure[preempt]{
    %\label{fig:subfig:b}%
    \includegraphics[width=5.8cm, height=4cm]{figures//preempt.pdf}
  }
  \caption{Different parallel algorithms: guided and preempt.}
  \label{figure4}
\end{figure}

\subsection{SPMP vs. EMS}
Extended Memory Semantics (EMS) is another model that addresses the challenges of parallel programming, including shared objects, synchronization, persistence and load-balancing, as we solved in our model. As far as we know, EMS is the only model that implements shared persistent memory parallelism except for \emph{SPMP}.

This experiment is conducted with four processes for five cases: EMS with \emph{``guided"}, EMS with \emph{``static"}, \emph{SPMP} with \emph{``guided"}, \emph{SPMP} with \emph{``preempt"} and \emph{SPMP} with \emph{``static"}. In the figure, x-axis is different operations, and y-axis is base-10 logarithm of the values of atomic operations per second. \emph{SPMP} takes a different way of memory allocating and mapping from EMS, and it works well according to the experiment results. For all the five operations, \emph{SPMP} is 100 - 500 times faster than EMS. As to the three different load-balancing strategies of \emph{SPMP}, \emph{``static"} outperforms the other two.

\begin{figure}
    \centerline{ \includegraphics[width=10cm, height=7cm]{figures//compare.pdf}}
    \caption{Compare different strategies of EMS and SPMP.}
    \label{figure5}
\end{figure}

\section{Conclusions}
In this paper, we studied shared persistent memory and parallel programming for Node.js. The main technique challenge is to map the shared memory and coordinate different processes. We develop a parallel programming model called \emph{SPMP} based on our newly designed Node.js interface PersistentArrayBuffer, and implement  persistence to both persistent memory and secondary storages. \texttt{child\_process} and bulk synchronous parallel model are used to design the parallel mechanism. In addition, with help of Native Abstractions for Node.js (nan), we realize concurrency control to solve the problems in process communication. We evaluate \emph{SPMP} with five basic operations, and compare it with Extended Memory Semantics. Our experiments demonstrate that \emph{SPMP} takes fully use of CPU cores and other resources, and coordinates multiple processes efficiently. It outperforms EMS by a factor of 100 - 500 times.

\bibliographystyle{splncs}
\bibliography{ref}

\end{document}
